package IDS::Snort::Rules::Parser;
use Moose;

# ABSTRACT: Parse Snort rule files for IDS::Snort

with 'MooseX::Log::Log4perl';

use Archive::Tar;
use Carp;
use IO::All;
use Parse::Snort::Strict;
use Text::Continuation::Parser qw(parse_line);
use Try::Tiny;

use IDS::Snort::Types qw(
    SnortRuleVersion
    SnortDistribution
);

has version => (
    is      => 'ro',
    isa     => SnortRuleVersion,
    default => 2,
);

has distro => (
    is        => 'ro',
    isa       => SnortDistribution,
    predicate => 'has_distro',
);

has gid => (
    isa     => 'HashRef',
    traits  => ['Hash'],
    handles => {
        get_gid => 'get',
        has_gid => 'defined',
        set_gid => 'set',
        del_gid => 'clear',
        ls_gid  => 'keys',
    },
    default => sub { {} },
);

has ignores => (
    isa     => 'ArrayRef',
    traits  => ['Array'],
    handles => {
        set_ignore  => 'get',
        all_ignores => 'elements',
    },
    default => sub { return [] },
);

has rules => (
    isa     => 'HashRef',
    traits  => ['Hash'],
    handles => {
        get_rule => 'get',
        has_rule => 'defined',
        set_rule => 'set',
        del_rule => 'clear',
        list  => 'keys',
    },
    default => sub { {} },
);

sub parse_rule {
    my ($self, $rule) = @_;

    my $rule_object = Parse::Snort::Strict->new;
    try {
        $rule_object->parse($rule);
    }
    catch {
        $self->log->warn("Unable to parse rule: $rule");
    };

    if (!$rule_object->sid) {
        $self->log->error("Rule '$rule' has no sid");
        return;
    }

    return $self->process_rule($rule_object);
}

sub process_rule {
    my ($self, $rule_object) = @_;

    if ($self->has_rule($rule_object->sid)) {
        $self->log->trace(sprintf("Rule with sid '%d' is already defined!", $rule_object->sid));
        $self->get_rule($rule_object->sid);
    }

    if (!$rule_object->gid) {
        # The gid keyword (generator id) is used to identify what part
        # of Snort generates the event when a particular rule fires. For
        # example gid 1 is associated with the rules subsystem and
        # various gids over 100 are designated for specific
        # preprocessors and the decoder. See etc/generators in the
        # source tree for the current generator ids in use. Note that
        # the gid keyword is optional and if it is not specified in a
        # rule, it will default to 1 and the rule will be part of the
        # general rule subsystem. To avoid potential conflict with gids
        # defined in Snort (that for some reason aren't noted it
        # etc/generators), it is recommended that values starting at
        # 1,000,000 be used. For general rule writing, it is not
        # recommended that the gid keyword be used. This option should
        # be used with the sid keyword.
        $rule_object->gid(1);
    }

    # TODO: Figure out what this does in IDS::Snort
    # and perhaps patch Snort::Parse to include the data.
    # Grab all the flow bits
    my @flowbits = grep { $_->[0] eq 'flowbits' } @{ $rule_object->get('opts') };
    foreach (@flowbits) {
        # my ($flowact, $flowbit); # but this now warns unusedvar.t in
        # author tests
        my ($flowact, undef) = split(/,/, $_);
        next unless $flowact =~ /^\s*((un)?set(x)?|toggle)/i;
    }

    $self->set_rule($rule_object->sid, $rule_object);
    $self->set_gid($rule_object->gid, $rule_object->sid);
    return $rule_object;
}

sub parse_rule_file {
    my ($self, $fh) = @_;

    while (defined(my $rule = parse_line($fh))) {
        $self->parse_rule($rule);
    }
    return 1;
}

sub _create_ignores {
    my ($self, $ignores) = @_;

    foreach (@$ignores) {
        if ($_ =~ /\.rules$/) {
            $self->set_ignore(qr#rules/$_#);
        }
        elsif ($_ =~ /\.preproc$/) {
            my $preproc = $_;
            $preproc =~ s/\.preproc/\.rules/;
            $self->set_ignore(qr#preproc_rules/$preproc#);
        }
        elsif ($_ =~ /\.so$/) {
            $self->set_ignore(qr#so_rules/precompiled/.*/$_#);
        }
        else {
            # ignore all the rules
            $self->set_ignore(qr#rules/$_#);
            $self->set_ignore(qr#preproc/$_#);
            $self->set_ignore(qr#so_rules/precompiled/.*/$_#);
        }
    }
}

my $filter = qr/\.rules$/;
#my $filter = qr/\.(?:rules|so)$/;

sub parse_rule_tgz {
    my ($self, $path) = @_;

    my $tar = Archive::Tar->iter($path, 1, { filter => $filter });

    while (my $f = $tar->()) {

        my $name = $f->name;

        #next if first { $name =~ /$_/ } @{$self->all_ignores};

        my $fh = io('?');
        $self->log->trace("Parsing file $name");
        $fh->print($f->get_content);
        $fh->seek(0,0);
        $self->parse_rule_file($fh);
    }

    return 1;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 DESCRIPTION

This module parses Snort Rules and processes rules for IDS::Snort. It
works with rules of Snort version 2 and Snort version 3 rule files.
1G

=head1 SYNOPSIS

    use IDS::Snort::Rules::Parser;

    my $parser = IDS::Snort::Rules::Parser->new(
        version => 2
    );

=head1 METHODS

=head2 parse_rule_tgz($path)

    my $parser = IDS::Snort::Rules::Parser->new();
    my $path = '/path/to/tarball.tgz';
    $parser->parse_rule_tgz($path);

Extracts a tarball and parses all the files found in this tarball.

=head2 parse_rule_file($filehandle)

    my $parser = IDS::Snort::Rules::Parser->new();
    open my $fh, '<', '/path/to/file';
    $parser->parse_rule_file($fh);

Parses a complete plain text rule file based on an object that supports
C<get_line>.

=head2 parse_rule

    my $parser = IDS::Snort::Rules::Parser->new();
    my $rule = {alert tcp $EXTERNAL_NET any -> $TELNET_SERVERS 23 (msg: foo; reference:pod; sid:1000000; rev:1;)};
    $parser->parse_rule($fh);

Parses an individual rule

=head2 process_rule

    my $parser = IDS::Snort::Rules::Parser->new();
    my $rule = Parse::Snort->new(); # or Parse::Snort::Strict->new();
    parser->process_rule($rule);

Process the rule, applies logic
