package IDS::Snort::Rules;
use Moose;
use namespace::autoclean;

# ABSTRACT: Snort tarballs and the likes
#
use Digest::MD5::File qw(file_md5_base64);

has name => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

has checksum => (
    is        => 'ro',
    isa       => 'Str',
    predicate => 'has_checksum',
);

has check_checksum => (
    is      => 'ro',
    isa     => 'Bool',
    default => 1,
);

has filehandle => (
    is        => 'ro',
    isa       => 'File::Temp',
    predicate => 'has_fh',
);

has filename => (
    is => 'ro',
    isa => 'Str',
    predicate => 'has_filename',
);

sub assert_checksum {
    my ($self) = @_;

    if (!$self->has_fh) {
        die sprintf("Unable to check the checksum of %s, we don't have a filehandle!", $self->name), $/
    }

    if (!$self->has_checksum) {
        die sprintf("Unable to check the checksum of %s, we don't have a checksum to check against!", $self->name), $/
    }

    my $d = Digest::MD5::File->new();
    $d->addpath($self->filehandle->filename);
    my $digest = $d->b64digest;

    return 1 if $digest eq $self->checksum;

    die sprintf("Checksum %s does not match %s for %s!", $self->name, $digest, $self->checksum), $/
}


__PACKAGE__->meta->make_immutable;

__END__

=head1 DESCRIPTION

=head1 SYNOPSIS

=head1 ATTRIBUTES

=head1 METHODS

=head2 assert_checksum

Asserts wheter the checksum of the file and the checksum received are the same.

