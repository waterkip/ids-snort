package IDS::Snort::Types;
use warnings;
use strict;

# ABSTRACT: Types in use by IDS::Snort

use Moose::Util::TypeConstraints qw(enum);

use MooseX::Types -declare => [
    qw(
        SnortVersion
        SnortMajorVersion
        SnortRuleVersion
        SnortDistribution
    )
];

subtype SnortVersion, as enum([qw(
        2.9.2
        2.9.7
        2.9.9
        3.0.0
    )]),
    message { "'$_' is not a valid Snort version number" };

subtype SnortMajorVersion, as enum([qw(
        2.9
        3.0
    )]),
    message { "'$_' is not a valid Snort major version number" };

subtype SnortRuleVersion, as enum([qw(
        2
        3
    )]),
    message { "'$_' is not a valid Snort rule version number" };

subtype SnortDistribution, as enum([qw(
        Centos
        Debian
        FC
        FreeBSD
        OpenBSD
        OpenSUSE
        RHEL
        Slackware
        Ubuntu
    )]),
    message { "'$_' is not a valid Snort distribution number" };

1;

__END__

=head1 DESCRIPTION

This module describes the custom types used by L<IDS::Snort>.

=head1 SYNOPSIS

    package IDS::Snort::Foo;
    use Moose;
    use IDS::Snort::Types qw(SnortVersion);

    has foo => (
        is => 'ro',
        isa => SnortVersion,
    );

=head2 SnortVersion

Supported Snort versions: C<2.9.2>, C<2.9.7>, C<3.0>

=head2 SnortMajorVersion

Supported Snort major versions: C<2.9> and C<3.0>

=head2 SnortRuleVersion

Supported rule versions for Snort: C<2> and C<3>

=head2 SnortDistribution

Supported distributions for compiling dynamic rules: C<Centos>,
C<Debian>, C<FC>, C<FreeBSD>, C<OpenBSD>, C<OpenSUSE>, C<RHEL>,
C<Slackware> and C<Ubuntu>

