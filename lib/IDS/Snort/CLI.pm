package IDS::Snort::CLI;
use Moose;

# ABSTRACT: CLI client for IDS Snort

with qw(
    MooseX::Log::Log4perl
);

use Carp qw(croak);
use Getopt::Long qw(GetOptionsFromArray :config bundling auto_version);
use Try::Tiny;
use Pod::Usage;
use Log::Log4perl qw(:easy);

use IDS::Snort::Config;
use IDS::Snort;

use Data::Dumper;

has ids => (
    is       => 'ro',
    isa      => 'IDS::Snort',
    required => 1,
);

has command_line_arguments => (
    is       => 'ro',
    isa      => 'HashRef',
    required => 1,
    reader   => 'options',
);

sub command_line_options {
    return qw(
        dry|n!
        v+
        q+
        help|?
        config|c=s
        download|dl!
        local|l!
        msg|m!
        parse|p!
        md5!
    );
}

sub _init_log {
    my %args = @_;

    # TODO:
    #
    # Instantiate logger as quickly as possible if found in config, then
    # use that and set the loglevel else use easy_init (or maybe some
    # default sanish log4perl config)
    #
    my $level;
    if (my $q = $args{q}) {
        if ($q == 1) {
            $level = $ERROR;
        }
        elsif ($q == 2) {
            $level = $FATAL;
        }
        else {
            $level = $OFF;
        }
    }
    elsif (my $v = $args{v}) {
        if ($v == 1) {
            $level = $INFO;
        }
        elsif ($v == 2) {
            $level = $DEBUG;
        }
        else {
            $level = $TRACE;
        }
    }

    Log::Log4perl->easy_init($level // $WARN);

}

sub init {
    my ($self, $args) = @_;

    if (defined $args && ref $args ne 'ARRAY') {
        croak "Args is not an array";
    }

    $args //= \@ARGV;
    my %cli_args;

    {
        local $SIG{__WARN__};

        my @spec = command_line_options();

        try {
            GetOptionsFromArray($args, \%cli_args, @spec)
        }
        catch {
            die $_;
        };
    }

    # In case you init, we assume you are a CLI script in that case, don't roll
    # back automaticly
    $cli_args{dry} //= 0;

    _init_log(%cli_args);

    if ($cli_args{help}) {
        pod2usage(-verbose => 1, -exitval => 0);
    }

    my $config = delete $cli_args{config};
    if ($config) {
        $config = IDS::Snort::Config->new(path => $config);
    }
    else {
        $config = IDS::Snort::Config->new();
    }

    # TODO: Fix this?
    # $config->merge_into(%cli_args);

    my $ids = IDS::Snort->new(
        config => $config,
    );

    return $self->new(
        ids                    => $ids,
        command_line_arguments => \%cli_args,
    );
}

sub run {
    my $self = shift;

    if (!$self->ids->config->has_path) {
        # TODO: Ask user to generate config
        # if yes
        # generate config and quit
    }

    if ($self->options->{download}) {
        $self->ids->download_rules();
    }

    if ($self->options->{local}) {
        $self->ids->get_local_rules();
    }

    if ($self->options->{parse}) {
        if (!$self->options->{download}) {
            $self->ids->downloaded_rules();
        }
        $self->ids->parse_rules();
    }

    if ($self->options->{msg}) {
        $self->ids->msg_map();
    }

    if (!$self->options->{dry}) {
        $self->ids->write_output();
    }
    else {
        $self->ids->dump_output();
    }

}

__PACKAGE__->meta->make_immutable;

__END__

=head1 DESCRIPTION

=head1 SYNOPSIS

    #!/usr/bin/perl
    use warnings;
    use strict;

    #
    # ABSTRACT: A Snort command line helper script
    # PODNAME: snort-cli.pl
    #
    our $VERSION = $IDS::Snort::VERSION;

    use IDS::Snort::CLI;

    my $cli = IDS::Snort::CLI->init();
    $cli->run();

    1;

=head1 METHODS

=head2 command_line_options

Returns all the command line options for use of Getopt::Long

=head2 init

Initialises the CLI application. When no arguments are given the CLI
application parses everything from the command line (C<@ARGV>).

    my $cli = IDS::Snort::CLI->init();

    # or

    my $download = IDS::Snort::CLI->init([qw(--download)]);

=head2 run

Run the command line application
