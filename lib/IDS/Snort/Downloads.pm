package IDS::Snort::Downloads;
use Moose;

with qw(
    MooseX::Log::Log4perl
);

# ABSTRACT: Download snort (compatible) rules from snort.org and others

use IO::All;
use File::Temp;
use HTTP::Request;
use URI;

#use IDS::Snort::Types qw(HTTP_METHODS);

with 'MooseX::Log::Log4perl';

has name => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

has ua => (
    is       => 'ro',
    isa      => 'LWP::UserAgent',
    required => 0,
);

has oinkcode => (
    is      => 'ro',
    isa     => 'Str',
    default => '',
);

has uri => (
    is       => 'rw',
    isa      => 'Str',
    required => 1,
);

has check_checksum => (
    is      => 'ro',
    isa     => 'Bool',
    default => 1,
);

has method => (
    is      => 'ro',
    isa     => 'Str',
    default => 'GET',
);

sub transform_uri {
    my ($self, $md5_file) = @_;

    # take the base uri
    # and transform it to something else via URI

    #if ($uri =~ /[^labs]\.snort\.org/i) {
    #    $rule_uri = "https://www.snort.org/rules/$md5_file\?oinkcode="
    #        . $self->oinkcode;
    #}
    return sprintf("https://www.snort.org/rules/%s\?oinkcode=%s", $md5_file, $self->oinkcode);

}

sub download_checksum {
    my ($self) = @_;

    my $uri  = URI->new($self->uri);
    my $file = $uri->path;
    my $md5  = "$file.md5";

    my $rule_uri = $self->transform_uri($md5);

    $self->log->debug("Retreiving MD5 from uri $rule_uri");

    my $request = HTTP::Request->new($self->method => $rule_uri);
    my $response = $self->ua->request($request);
    $self->_assert_response($response);
    return $response->decoded_content;
}

sub _assert_response {
    my ($self, $response) = @_;

    return 1 if $response->is_success;

    if ($self->log->is_trace) {
        $self->log->trace("Request\n:" . $response->request->as_string);
        $self->log->trace("Response\n:" . $response->as_string);
    }

    # TODO: 4XX catching
    # TODO: Remove oinkcode from URI
    my $msg = sprintf(
        "Error downloading %s: %s [ %d ]",
        $response->request->uri,
        $response->status_line, $response->code
    );
    die $msg, $/;
}

sub download_rule {
    my $self = shift;

    my $uri = $self->uri;

    $self->log->trace("Downloading file from $uri");

    $uri = URI->new($uri);
    my $file = File::Temp->new();

    my $request  = HTTP::Request->new($self->method => $uri);
    my $response = $self->ua->request($request, $file->filename);

    $self->_assert_response($response);
    return $file;
}

sub run {
    my $self = shift;

    # TODO:
    if ($self->check_checksum) {
        $self->download_checksum();
    }

    $self->download_rule;
}

__PACKAGE__->meta->make_immutable;

__END__

