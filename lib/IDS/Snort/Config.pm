package IDS::Snort::Config;
use Moose;

use Config::General;
use Data::Dumper;

has path => (
    is        => 'ro',
    isa       => 'Str',
    predicate => 'has_path',
);

has config => (
    is      => 'ro',
    isa     => 'Config::General',
    builder => '_build_config',
    lazy    => 1,
    reader  => '_config'
    #handles => [qw(oinkcode)],
);

sub _build_config {
    my $self = shift;

    if ($self->has_path) {
        return Config::General->new(
            -ConfigFile      => $self->path,
            -InterPolateVars => 1,
            -InterPolateEnv  => 1,
            -ForceArray      => 1,
            -AutoTrue        => 1,
            -ExtendedAccess  => 1,
        );
    }
}

sub merge_into {
    my ($self, %options) = @_;
    ...;
}

sub _get_config {
    my $self = shift;
    return $self->_config->{DefaultConfig};
}

sub get_downloads {
    my $self = shift;

    if(my $downloads = $self->_get_config->{download}) {
        if (ref $downloads eq 'HASH') {
            return $downloads;
        }
        else {
            die "Unable to read downloads from config. Please check the syntax", $/;
        }
    }

    return {};
}

sub download_may_fail {
    my $self = shift;
    return 0;
}

__PACKAGE__->meta->make_immutable;

__END__

