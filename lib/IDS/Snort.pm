use utf8;
package IDS::Snort;
our $VERSION = '0.001';
use Moose;
use namespace::autoclean;
use Data::Dumper;
use File::Spec::Functions qw(catfile);

# ABSTRACT: A Snort rule manager

with qw(
    MooseX::Log::Log4perl
);

use IDS::Snort::Downloads;
use IDS::Snort::Rules::Parser;

has config => (
    is       => 'ro',
    isa      => 'IDS::Snort::Config',
    required => 1,
);

has rule_files => (
    is      => 'ro',
    isa     => 'HashRef',
    traits  => ['Hash'],
    handles => {
        get_rule_file  => 'get',
        set_rule_file  => 'set',
        list_rule_file => 'keys',
    },
    default => sub { {} },
);

has parser => (
    is => 'ro',
    isa => 'IDS::Snort::Rules::Parser',
    default => sub {
        return IDS::Snort::Rules::Parser->new();
    },
);

has some_map => (
    is => 'rw',
    isa => 'Str',
    predicate => 'has_msg_map',
);

sub download_rules {
    my $self = shift;

    $self->log->trace("Downloading rules");
    my $downloads = $self->config->get_downloads();

    foreach (keys %$downloads) {

        $self->log->trace("Create downloader for $_");

        # Downloader returns IDS::Snort::Downloads::$module_name
        my $downloader = IDS::Snort::Downloads->new(
            name => $_,
            %{ $downloads->{$_} },
        );

        my $rule = $downloader->fetch();
        $self->set_rule_file($rule->name, $rule);

    }
    return;
}

sub get_local_rules {
    my $self = shift;
    my $local = $self->config->get_local_rules();

    foreach (keys %$local) {
        $self->log->trace("Create local fetcher for $_");
        my $fetcher = IDS::Snort::Rules::Local->new(
            name => $_,
            path => $local->{path},
        );

        my $rule = $fetcher->fetch();
        $self->set_rule_file($rule->name, $rule);
    }
}

sub get_downloaded_rules {
    my $self = shift;

    my $downloads = $self->config->get_downloads();

    my $basedir = $self->config->rule_dir_storage;

    foreach (keys %$downloads) {

        $self->log->trace("Create local fetcher for $_");

        my $fetcher = IDS::Snort::Rules::Local->new(
            name => $_,
            path => catfile($basedir, $downloads->{filename}),
        );

        my $rule = $fetcher->fetch();
        $self->set_rule_file($rule->name, $rule);
    }
}

sub parse_rules {
    my $self = shift;

    $self->log->trace("Parsing rules");
    my @rules = $self->list_rule_file();

    foreach (keys @rules) {
        $self->log->trace("Parsing and processing rules for '$_'");
        my $rules = $self->get_rule_file($_);

        if ($rules->rules =~ /\.tgz$/) {
            $self->parser->parse_rule_tgz($rules->path);
        }
        elsif ($rules->rules =~ /\.rules?$/) {
            $self->parser->parse_rule_file($rules->filehandle);
        }
        else {
            my $msg = "Unable to process rules, do not know how to process rule extension!";
            $self->log->error($msg);
            die $msg, $/;
        }
    }
    return;
}

sub msg_map {
    my $self = shift;
    # TODO: generate the msg map
    ...;
}

sub write_output {
    my ($self, $include_inactive) = @_;
    my $fh;

    $self->_write_rules($fh, $include_inactive);
    $self->_write_rules($fh, $include_inactive);

    if ($self->has_msg_map) {
        $self->log->info("Printing msg map");
    }
}

sub downloaded_rules {
    my $self = shift;
    ...;
}

sub _write_rules {
    my ($self, $fh, $include_inactive) = @_;

    my @rules = $self->parser->list;
    foreach (@rules) {
        my $rule = $self->parser->get_rule($_);
        next if !$include_inactive && !$rule->state;

        if ($fh) {
            print $fh $rule->as_string;
        }
        else {
            print $rule->as_string;
        }
    }
}

sub _write_msg_map {
    my ($self, $fh) = @_;

    if ($self->has_msg_map) {
        $self->log->debug("Msg map generated");

        if ($fh) {
            print $fh $self->msg_map;
        }
        else {
            print $self->msg_map;
        }
    }
    else {
        $self->log->debug("No msg map generated");
    }
}

sub dump_output {
    my ($self, $include_inactive) = @_;

    $self->_write_rules(undef, $include_inactive);

    my @rules = $self->parser->list;
    foreach (@rules) {
        my $rule = $self->parser->get_rule($_);
        next if !$include_inactive && !$rule->state;
        print $rule->as_string;
    }
};



__PACKAGE__->meta->make_immutable;

__END__

=head1 DESCRIPTION

IDS::Snort provides boar.pl which is inspired by
L<PulledPork|https://github.com/shirkdog/pulledpork>. The script itself
however is just several lines of code instead of the many lines
PulledPork has.

It does so by providing several modules in the IDS::Snort namespace that
take care of configuration parsing, rule parsing and manipulation, and
so forth.

You could create your own snort manager scripts by using L<IDS::Snort::CLI>.

=head1 METHODS

=head2 download_rules

Download all the rules based on the configuration files.

=head2 parse_rules

Parse all the rules based on the configuration files.

