#!/usr/bin/perl
use warnings;
use strict;

#
# ABSTRACT: A Snort Rule manager
# PODNAME: boar.pl
#
our $VERSION = $IDS::Snort::VERSION;

use IDS::Snort::CLI;

my $cli = IDS::Snort::CLI->init();

$cli->run();

1;

__END__

=head1 DESCRIPTION

boar.pl is a Snort rule manager. You can download, generate, update,
delete, modify update rules for Snort. It is similar to pulledpork, but
testable and modular.

=head1 SYNOPSIS

    boar.pl --help [ OPTIONS ]

=head1 OPTIONS

=over

=item * --help

This help

=item * --version

Show the version number

=item * --download

Download the rules

=item * --process

Process all the rules

=item * --no-md5

Disable md5 checksum checks. One is not advised to do so.

=back
