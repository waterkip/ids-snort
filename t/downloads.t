use lib qw(t/lib);
use IDS::Snort::Test;

use IDS::Snort::Downloads;
use File::Copy;
use LWP::UserAgent;
use HTTP::Response;
use Path::Tiny;


{

    # TODO: disable on no network testing

    my $ua = LWP::UserAgent->new();

    my $override = Sub::Override->override(
        'LWP::UserAgent::request' => sub {
            HTTP::Response->new('200', undef, undef, "123456789");
        }
    );

    my $uri = "https://snort.org/downloads/community/community-rules.tar.gz";

    my $dl = IDS::Snort::Downloads->new(
        name      => "Testsuite",
        ua        => $ua,
        uri       => $uri,
        directory => 't/inc/tmp',
    );

    is($dl->download_checksum(), '123456789');

    my $file = 't/downloads.t';
    $file = path($file);
    my $contents = $file->slurp;

    $override = Sub::Override->override(
        'LWP::UserAgent::request' => sub {
            my $ua = shift;
            my $request = shift;
            my $r = HTTP::Response->new('200', undef, undef, $contents);
            $r->request($request);
            $r->content_type('application/x-gzip');
            return $r;
        }
    );

    my $rule = $dl->download_rule();
    isa_ok($rule, "File::Temp");
    #my $download = path($rule->filename);
    #is($download->slurp, $contents, "Contents and download are the same");
    #move($rule->filename, "tmp/foo.zip");


}

done_testing();
