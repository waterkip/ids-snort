use lib qw(t/lib);
use IDS::Snort::Test;

use Archive::Tar;
use IDS::Snort::Rules::Parser;
use IO::All;
use Log::Log4perl::Level;

sub get_rules {
    my $rules = IDS::Snort::Rules::Parser->new(@_);
    isa_ok($rules, "IDS::Snort::Rules::Parser");
    return $rules;
}

note "Simple rule parsing";
my $rule = get_rule();

my $rules = get_rules();
my $t = $rules->parse_rule($rule);
isa_ok($t, "Parse::Snort");

my $fh = io('?');
$fh->print($rule, $/);
$fh->seek(0,0);

$rules = get_rules();
$rules->parse_rule_file($fh);

my @keys = $rules->list;
is(@keys, 1, "Found one rule after processing");

cmp_deeply($rules->get_rule($keys[0]), $t, "Rule matches");

done_testing;
