use lib qw(t/lib);
use IDS::Snort::Test;

if (!$ENV{IDS_SNORT_RULES}) {
    plan skip_all => 'Skipping run on live snort files';
}

use IDS::Snort::Rules::Parser;

my $rules = IDS::Snort::Rules::Parser->new();

my @archives = qw(community-rules snortrules-snapshot-2990);

foreach (@archives) {
    my $filename = "rules/$_.tar.gz";
    lives_ok(
        sub {
            $rules->parse_rule_tgz($filename);
        },
        "Parsing rules $filename",
    );
}

done_testing;
