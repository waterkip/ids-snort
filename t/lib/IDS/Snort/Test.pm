package IDS::Snort::Test;
use strict;
use warnings;

# ABSTRACT: A base test pacakge for IDS::Snort

use namespace::autoclean ();

use Import::Into;
use Test::Fatal;
use Test::Most;
use Sub::Override;

sub import {

    my $caller_level = 1;

    # Test::Most imports *ALL* functions of Test::Deep, Test::Deep has
    # any, all, none, and some others that List::Utils also has.
    # Test::Deep has EXPORT_TAGS but they include pretty much everything
    my @TEST_DEEP_LIST_UTILS = qw(!any !all !none);
    Test::Most->import::into($caller_level, @TEST_DEEP_LIST_UTILS);

    my @imports = qw(
        namespace::autoclean
        Test::Fatal
        IDS::Snort::Testdata
    );

    $_->import::into($caller_level) for @imports;
}

1;

__END__

=head1 DESCRIPTION

Imports all the stuff we want plus sets strict/warnings etc

=head1 SYNOPSIS

    use lib qw(t/lib);
    use IDS::Snort::Test;

    # tests here

    done_testing;

=head1 AUTHOR

Wesley Schwengle

=head1 LICENSE and COPYRIGHT

Wesley Schwengle, 2018.
