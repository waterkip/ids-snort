package IDS::Snort::Testdata;
use strict;
use warnings;

require Exporter;
our @ISA = qw(Exporter);
our @EXPORT = qw($logger get_rule);

use Log::Log4perl qw(:easy);

Log::Log4perl->easy_init($OFF);
our $logger = Log::Log4perl->get_logger();

sub get_rule {
    return q{alert tcp $EXTERNAL_NET any -> $TELNET_SERVERS 23 (msg:"TELNET login buffer non-evasive overflow attempt"; flow:to_server,established; flowbits:isnotset,ttyprompt; content:"|FF FA|'|00 00|"; rawbytes; pcre:"/T.*?T.*?Y.*?P.*?R.*?O.*?M.*?P.*?T/RBi"; flowbits:set,ttyprompt,foo; reference:bugtraq,3681; reference:cve,2001-0797; classtype:attempted-admin; sid:3274; rev:3;)};

};


1;

__END__

=head1 NAME

IDS::Snort::TestData - Set test data

=head1 DESCRIPTION

=head1 SYNOPSIS

=head1 AUTHOR

Wesley Schwengle

=head1 LICENSE and COPYRIGHT

Wesley Schwengle, 2017.

