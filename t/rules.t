use lib qw(t/lib);
use IDS::Snort::Test;

use IDS::Snort::Rules;
use File::Temp;

sub get_rules {
    my $rules = IDS::Snort::Rules->new(name => 'test', @_);
    isa_ok($rules, "IDS::Snort::Rules");
    return $rules;
}

my $checksum = '1B2M2Y8AsgTpgAmY7PhCfg';
my $fh       = File::Temp->new();

throws_ok(
    sub {
        my $rules = get_rules();
        $rules->assert_checksum;
    },
    qr/Unable to check the checksum of test, we don't have a filehandle\!/,
    "->assert_checksum needs a filehandle"
);

throws_ok(
    sub {
        my $rules = get_rules(
            filehandle => $fh
        );
        $rules->assert_checksum;
    },
    qr/Unable to check the checksum of test, we don't have a checksum to check against\!/,
    ".. and needs a checksum too",
);

my $checksum_m = $checksum . "fo" ;
throws_ok(
    sub {
        my $rules = get_rules(
            checksum   => $checksum_m,
            filehandle => $fh
        );
        $rules->assert_checksum;
    },
    qr/Checksum test does not match $checksum for $checksum_m\!/,
    ".. and the checksum needst to match",
);

lives_ok(
    sub {
        my $rules = get_rules(
            checksum   => $checksum,
            filehandle => $fh
        );
        $rules->assert_checksum;
    },
    ".. and returns true when both are ok"
);

done_testing;
