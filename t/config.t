use lib qw(t/lib);
use IDS::Snort::Test;

use IDS::Snort::Config;

my $path = $ENV{BOAR} || 'etc/boar.conf.dist';

my $ids = IDS::Snort::Config->new(
    path => $path,
);

my $config = $ids->_config;
isa_ok($config, "Config::General");

note explain $config;

if ($ENV{BOAR}) {
    #diag explain $config;
}
else {
    my $downloads = $ids->get_downloads;
    note explain $downloads;
}

done_testing;
