use lib qw(t/lib);
use IDS::Snort::Test;

use IDS::Snort::CLI;

my @args = (
    '--config' => 'etc/boar.conf.dist',
);


sub get_cli {
    my $cli = IDS::Snort::CLI->init(\@_);

    isa_ok($cli, 'IDS::Snort::CLI', "Instantiated a CLI");
    isa_ok($cli->ids, "IDS::Snort", ".. with an IDS object");
    isa_ok($cli->options, "HASH", ".. and we can get the cli arguments");

    return $cli;
}

{
    my $cli = get_cli(@args);
    cmp_deeply($cli->options, { dry => 0 }, "No arguments given");
    $cli->run;
}

{
    my $cli = get_cli(@args, '--download');
    cmp_deeply($cli->options, { download => 1, dry => 0 }, "Download was given given");

    my %called = ();
    my $override = _override(
        'IDS::Snort::download_rules' => sub { $called{download_rules}++; return {} },
    );
    my %expect = map { $_ => 1 }
        qw(download_rules);

    $cli->run;
    cmp_deeply(\%called, \%expect, "All functions are called");

}

{
    my $cli = get_cli(
        @args, qw(
            --download
            --local
            --parse
            --msg
        )
    );
    cmp_deeply($cli->options, { msg => 1, parse => 1, local => 1, download => 1, dry => 0 }, "All options are parsed");

    my %called = ();

    my $override = _override(
        'IDS::Snort::download_rules' => sub { $called{download_rules}++; return {} },
        'IDS::Snort::get_local_rules' => sub { $called{get_local_rules}++; return {} },
        'IDS::Snort::downloaded_rules' => sub { $called{downloaded_rules}++; return {} },
        'IDS::Snort::parse_rules' => sub { $called{parse_rules}++; return {} },
        'IDS::Snort::msg_map' => sub { $called{msg_map}++; return {} },
        'IDS::Snort::write_output' => sub { $called{write_output}++; return {} },
        'IDS::Snort::dump_output' => sub { $called{dump_output}++; return {} },
    );

    my %expect = map { $_ => 1 }
        qw(download_rules get_local_rules parse_rules msg_map write_output);


    $cli->run;
    cmp_deeply(\%called, \%expect, "All functions are called");
}


{
    my $cli = get_cli(
        @args, qw(
            --local
            --parse
            --msg
            --dry
        )
    );
    cmp_deeply($cli->options, { msg => 1, parse => 1, local => 1, dry => 1 }, "And all options are parsed");

    my %called = ();

    my $override = _override(
        'IDS::Snort::download_rules' => sub { $called{download_rules}++; return {} },
        'IDS::Snort::get_local_rules' => sub { $called{get_local_rules}++; return {} },
        'IDS::Snort::downloaded_rules' => sub { $called{downloaded_rules}++; return {} },
        'IDS::Snort::parse_rules' => sub { $called{parse_rules}++; return {} },
        'IDS::Snort::msg_map' => sub { $called{msg_map}++; return {} },
        'IDS::Snort::write_output' => sub { $called{write_output}++; return {} },
        'IDS::Snort::dump_output' => sub { $called{dump_output}++; return {} },
    );

    my %expect = map { $_ => 1 }
        qw(downloaded_rules get_local_rules parse_rules msg_map dump_output);


    $cli->run;

    cmp_deeply(\%called, \%expect, "All functions are called");
}

{
    my $cli = get_cli()
}

{
    my $pod = 0;
    my $override = _override(
        'IDS::Snort::CLI::pod2usage' => sub { $pod++; return 1; },
    );
    my $cli = get_cli('--help');

    is($pod, 1, "pod2usage called");
}

{
    throws_ok(
        sub {
            IDS::Snort::CLI->init({ illegal => 'formation' });
        },
        qr/Args is not an array/,
        "Requires arrayref"
    );
    lives_ok(
        sub {
            IDS::Snort::CLI->init(undef);
        },
        "Undef passing is ok",
    );
}


{
    my @v = ('-v');
    IDS::Snort::CLI->init([join("", @v)]);
    foreach (qw(v v)) {
        push(@v, $_);
        IDS::Snort::CLI->init([join("", @v)]);
    }

    my @q = ('-q');
    IDS::Snort::CLI->init([join("", @q)]);
    foreach (qw(q q)) {
        push(@q, $_);
        IDS::Snort::CLI->init([join("", @q)]);
    }
}


sub _override {
    my %overrides = @_;
    my $override;
    foreach (keys %overrides) {
        if ($override) {
            $override->override($_ => $overrides{$_});
        }
        else {
            $override = Sub::Override->override($_ => $overrides{$_});
        }
    }
    return $override;
}

done_testing;
